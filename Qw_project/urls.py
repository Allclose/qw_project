"""
URL configuration for Qw_project project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,re_path
from qw_app.views import *
from django.views.generic import TemplateView   #直接返回页面

urlpatterns = [
    path('admin/', admin.site.urls),
    path('get_case/', get_case),
    #path('sava_duan/',sava_duan),
    path('index/',TemplateView.as_view(template_name='index.html')),
]
#后端只需要调用dist/index.html即可
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import JsonViewer from 'vue-json-viewer'
import axios from "axios";

Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.use(JsonViewer)      //Json数据格式化展示
axios.defaults.baseURL = process.env.VUE_APP_BASE_URL    //axios默认url获取全局文件内的变量VUE_APP_BASE_URL


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

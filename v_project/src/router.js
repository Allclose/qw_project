import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Case_list from './views/Case_list'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home'
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
      {
      path: '/get_case',
      component: Case_list
    },
  ]
})

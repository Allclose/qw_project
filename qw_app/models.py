from django.db import models
import time
# Create your models here.

class Db_projects(models.Model):
    name = models.CharField(max_length=200,null=True,blank=True,default='-')
    creater = models.CharField(max_length=200, null=True, blank=True,default='-')
    des = models.CharField(max_length=500, null=True, blank=True,default='-')
    def __str__(self):
        return self.name

class Db_cases(models.Model):
    name = models.CharField(max_length=200,null=True,blank=True,default='-')
    creater = models.CharField(max_length=200, null=True, blank=True,default='-')
    create_time = models.CharField(max_length=200, null=True, blank=True, default=time.strftime('%Y-%m-%d %H:%M:%S'))
    script = models.CharField(max_length=20000,null=True,blank=True,default='-')
    des = models.CharField(max_length=500, null=True, blank=True,default='-')
    def __str__(self):
        return self.name
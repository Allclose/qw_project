from django.apps import AppConfig


class QwAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'qw_app'

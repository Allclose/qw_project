from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from qw_app.models import *
import json

# Create your views here.

def get_project(request):
    projects = list(Db_projects.objects.all())
    return HttpResponse(json.dumps(projects),content_type='application/json')
def get_case(request):
    cases = list(Db_cases.objects.all().values()[::-1])
    return HttpResponse(json.dumps(cases),content_type='application/json')
